import { json } from '@sveltejs/kit';



async function fetchValueCounts(selectedSubject) {
    const responseFetch = await fetch(
        `http://localhost:5173/api/statics/value-counts/?subject=${selectedSubject}`
    );

    if (responseFetch.ok) {
        const responseValue = await responseFetch.json()
        return responseValue;
    }
    else {
        console.error(
            "Respuesta no exitosa: Value Counts",
            responseFetch.statusText
        );
    }
    
}

async function fetchBinominalTest(valueCounts){

    const binominalTestResponse = await fetch(
        "http://localhost:5173/api/statics/binomial-test/",
        {
            method: "POST",
            body: JSON.stringify(valueCounts),
            headers: {
                "Content-Type": "application/json",
            },
        }
    );

    if (binominalTestResponse.ok) {
        const binomialTest = await binominalTestResponse.json();
        return binomialTest
        
    } else {
        console.error(
            "Respuesta no exitosa: Binominal Test",
            binominalTestResponse.statusText
        );
    }
}

async function fetchContingencyTable(selectedSubject){

    const responseFetch = await fetch(
        `http://localhost:5173/api/statics/contingency-table/?subject=${selectedSubject}`
    );

    if (responseFetch.ok) {
        const contingencyTable = await responseFetch.json();
        return contingencyTable
        
    } else {
        console.error(
            "Respuesta no exitosa Contingency Table:",
            responseFetch.statusText
        );
    }
}

async function fetchChiSquaredSideBias(contingencyTable){

    const chiSquaredSideBiasResponse = await fetch(
        "http://localhost:5173/api/statics/chi-squared/contingency",
        {
            method: "POST",
            body: JSON.stringify(contingencyTable),
            headers: {
                "Content-Type": "application/json",
            },
        }
    );

    if (chiSquaredSideBiasResponse.ok) {
        const chiSquaredSideBias = await chiSquaredSideBiasResponse.json();
        return chiSquaredSideBias
        
    } else {
        console.error(
            "Respuesta no exitosa: Chi Squared Side Bias",
            chiSquaredSideBiasResponse.statusText
        );
    }
}


async function fetchCorrelationTest(selectedSubject){
    const responseFetch = await fetch(
        `http://localhost:5173/api/statics/correlation/?subject=${selectedSubject}`
    );

    if (responseFetch.ok) {
        const correlationTest = await responseFetch.json();
        return correlationTest
        
    } else {
        console.error(
            "Respuesta no exitosa: Correlation Test",
            responseFetch.statusText
        );
    }
}


export async function GET({ url }) {
    const selectedSubjectType = String(url.searchParams.get('subject') ?? '');
    try {
        const valueCounts = await fetchValueCounts(selectedSubjectType);
        const binomialTest = await fetchBinominalTest(valueCounts);
        const contigencyTable = await fetchContingencyTable(selectedSubjectType)
        const chiSquaredSideBias = await fetchChiSquaredSideBias(contigencyTable)
        const correlationTest = await fetchCorrelationTest(selectedSubjectType)
    

        const response = {
            statics: {
                valueCounts,
                binomialTest,
                contigencyTable,
                chiSquaredSideBias,
                correlationTest
            }
        }
        return json(response);

    }
    catch (error) {
        console.log(error);
    }
}


import { json } from '@sveltejs/kit';

import binomialTest from '@stdlib/stats-binomial-test';



export async function POST({ request }) {
    try {
        const body = await request.json();
        const { data } = body;
        const { correct, incorrect } = data;

        const k_all = correct;
        const n_all = incorrect + correct;

        const significanceLevel = 0.05;

        const res = binomialTest(k_all, n_all);

        const existePreferencia = res.pValue  < significanceLevel;

        const response = {
            pValue: res.pValue,
            existePreferencia,
        };
        
        return json(response);

    } catch (error) {
        console.error(error);
        return {
            body: json({ error: error.message }),
            status: 500,
        };
    }
}

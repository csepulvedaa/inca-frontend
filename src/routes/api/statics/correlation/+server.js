// api/statics/correlation.js
import { json } from '@sveltejs/kit';
import { getFirestore, collection, query, where, getDocs } from 'firebase/firestore';
import { setCache, getCache } from '../../../../../src/lib/cache';

const CACHE_MAX_AGE = import.meta.env.VITE_CACHE_MAX_AGE; // 1 minute

export async function GET({ url }) {
  const selectedSubjectType = String(url.searchParams.get('subject') ?? '');
  const cacheKey = `correlation_${selectedSubjectType}`;

  // Try to get data from cache
  const cachedData = getCache(cacheKey, CACHE_MAX_AGE);
  if (cachedData) {
    return json(cachedData);
  }

  // If not in cache, fetch from Firestore
  const db = getFirestore();
  const q = query(collection(db, 'What-is-more1.0'), where('subject', '==', selectedSubjectType));
  const querySnapshot = await getDocs(q);
  const actions = querySnapshot.docs.map(doc => doc.data());

  const filteredActions = actions.filter((action) => {
    const isActionTypeMatch = action.action === 'Option Clicked';

    const isSubjectTypeMatch =
      selectedSubjectType === '' || action.subject === selectedSubjectType;

    // Ambas condiciones deben cumplirse para que la acción pase el filtro
    return isActionTypeMatch && isSubjectTypeMatch;
  });

  const data_list = calculateCorrelationData(filteredActions);

  // Now, you can create a DataFrame-like structure in JavaScript
  const new_df_all = data_list.map(([c0, c1, dot_sum, dot_dif, ratio, percentage, total]) => ({
    'Value 1': c0,
    'Value 2': c1,
    'Total': dot_sum,
    'Difference': dot_dif,
    'Ratio': ratio,
    'Accuracy': percentage,
    'Total Experiments': total,
  }));

  // Cache the response
  setCache(cacheKey, new_df_all);

  return json(new_df_all);
}

function calculateCorrelationData(actions) {
  const comb_list = getUniqueTwoItemCombinations(actions);
  const data_list = [];

  for (const item of comb_list) {
    const c0 = item[0];
    const c1 = item[1];

    const temp_df = actions.filter(
      (action) =>
        (action.details.options.c0 === c0 &&
          action.details.options.c1 === c1 && action.details.options.c2 === 'none') ||
        (action.details.options.c0 === c1 &&
          action.details.options.c1 === c0 && action.details.options.c2 === 'none')
    );

    if (temp_df.length !== 0) {
      const true_values = temp_df.reduce(
        (sum, action) => sum + action.details.result,
        0
      );
      const total = temp_df.length;
      const percentage = parseFloat((true_values / total) * 100).toFixed(2);
      const dot_sum = c0 + c1;
      const dot_dif = Math.abs(c0 - c1);
      const ratio = Math.min(c0, c1) / Math.max(c0, c1);

      data_list.push([c0, c1, dot_sum, dot_dif, ratio, percentage, total]);
    }
  }

  return data_list;
}

function getUniqueTwoItemCombinations(actions) {
  // Extract unique non-undefined values from options.c0, options.c1, ...
  const uniqueNonUndefinedValues = actions.reduce((acc, action) => {
    const { options } = action.details;
    Object.keys(options).forEach((key) => {
      if (options[key] !== "undefined") {
        acc.add(options[key]);
      }
    });
    return acc;
  }, new Set());

  // Convert the Set to an array
  const uniqueValuesArray = [...uniqueNonUndefinedValues];

  // Function to generate all unique two-item combinations
  const uniqueTwoItemCombinations = [];
  for (let i = 0; i < uniqueValuesArray.length; i++) {
    for (let j = i + 1; j < uniqueValuesArray.length; j++) {
      uniqueTwoItemCombinations.push([uniqueValuesArray[i], uniqueValuesArray[j]]);
    }
  }

  return uniqueTwoItemCombinations;
}

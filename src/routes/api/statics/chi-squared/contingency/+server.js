import { json } from '@sveltejs/kit';
import chiSquared from 'chi-squared';

function calculateChiSquareTest(contingencyTable) {
    // Step 1: Calculate row and column totals
    const rowTotals = {};
    const colTotals = {};
    let grandTotal = 0;
  
    Object.keys(contingencyTable).forEach(result => {
      rowTotals[result] = 0;
      Object.keys(contingencyTable[result]).forEach(side => {
        rowTotals[result] += contingencyTable[result][side];
        colTotals[side] = (colTotals[side] || 0) + contingencyTable[result][side];
        grandTotal += contingencyTable[result][side];
      });
    });
  
    // Step 2: Calculate expected frequencies
    const expectedFrequencies = {};
    Object.keys(contingencyTable).forEach(result => {
      expectedFrequencies[result] = {};
      Object.keys(contingencyTable[result]).forEach(side => {
        const expected = (rowTotals[result] * colTotals[side]) / grandTotal;
        expectedFrequencies[result][side] = expected;
      });
    });
  
    // Step 3: Calculate the chi-square test statistic
    let chiSquaredTestStatistic = 0;
    Object.keys(contingencyTable).forEach(result => {
      Object.keys(contingencyTable[result]).forEach(side => {
        const observed = contingencyTable[result][side];
        const expected = expectedFrequencies[result][side];
        chiSquaredTestStatistic += Math.pow(observed - expected, 2) / expected;
      });
    });
  
    // Step 4: Calculate degrees of freedom
    const degreesOfFreedom = (Object.keys(contingencyTable).length - 1) * (Object.keys(colTotals).length - 1);
  
    // Step 5: Calculate the p-value using the chi-square distribution
    const pValue = 1 - chiSquared.cdf(chiSquaredTestStatistic / 2, degreesOfFreedom / 2);
  
    // Return the result
    return {
      chiSquaredTestStatistic,
      degreesOfFreedom,
      pValue,
    };
  }


  
export async function POST({ request }) {
  try {
    const body = await request.json();


    const result = calculateChiSquareTest(body);

    // console.log('Chi-Squared Test Statistic:', result.chiSquaredTestStatistic);
    // console.log('P value', result.pValue);

    // console.log('Degrees of Freedom:', result.degreesOfFreedom);

    const significanceLevel = 0.05;
    const existePreferencia = result.pValue < significanceLevel;

    // console.log(existePreferencia);

    const response = {
      chiSquared: result.chiSquaredTestStatistic,
      pValue: result.pValue,
      degreesOfFreedom: result.degreesOfFreedom,
      existePreferencia: existePreferencia,
    };

    return json(response);
  } catch (error) {
    console.error(error);
    return null;
  }
  return;
}

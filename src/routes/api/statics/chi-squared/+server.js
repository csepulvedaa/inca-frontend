import { json } from '@sveltejs/kit';
import chiSquared from 'chi-squared';

function calculateChiSquare(data) {
    const observedValues = Object.values(data);
    const expected = observedValues.reduce((a, b) => a + b) / observedValues.length;

    const chiSquare = observedValues.reduce((chiSquare, observed) => {
        return chiSquare + ((observed - expected) ** 2) / expected;
    }, 0);

    const degreesOfFreedom = observedValues.length - 1;
    const pValue = 1 - chiSquared.cdf(chiSquare, degreesOfFreedom);

    return { chiSquare, pValue, degreesOfFreedom };
}

export async function POST({ request }) {
    try {
        const body = await request.json();
        const { data } = body;
        const { chiSquare, pValue, degreesOfFreedom } = calculateChiSquare(data);
        const significanceLevel = 0.05;

        const existePreferencia = pValue < significanceLevel;

        const response = {
            chiSquare,
            pValue,
            degreesOfFreedom,
            existePreferencia
        }
        return json(response);
    }
    catch (error) {
        console.error(error);
    }
    return;
}

import { json } from '@sveltejs/kit';
import { actions } from '../../../store/cio';

export async function GET({ url }) {
  const selectedActionType = String(url.searchParams.get('type') ?? '');
  const selectedSubjectType = String(url.searchParams.get('subject') ?? '');
  const selectedTeacherType = String(url.searchParams.get('user') ?? '');

  console.log(selectedActionType, selectedSubjectType, selectedTeacherType);

  const filteredActions = actions.filter((action) => {
    const isActionTypeMatch =
      selectedActionType === '' || action.action === selectedActionType;
    const isSubjectTypeMatch =
      selectedSubjectType === '' || action.subject === selectedSubjectType;
    const isUserTypeMatch =
      selectedTeacherType === '' || action.teacher === selectedTeacherType;

    // Ambas condiciones deben cumplirse para que la acción pase el filtro
    return isActionTypeMatch && isSubjectTypeMatch && isUserTypeMatch;
  });

  console.log(filteredActions.length)

  return json(filteredActions);
}


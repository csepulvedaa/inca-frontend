import { json } from '@sveltejs/kit';
import { actions } from '../../../../../store/whatismore';

export async function GET({ url }) {
  const selectedSubjectType = String(url.searchParams.get('subject') ?? '');

  const filteredActions = actions.filter((action) => {
    const isActionTypeMatch = action.action === 'Option Clicked';
    
    const isSubjectTypeMatch =
      selectedSubjectType === '' || action.subject === selectedSubjectType;

    // Ambas condiciones deben cumplirse para que la acción pase el filtro
    return isActionTypeMatch && isSubjectTypeMatch;
  });
  console.log(filteredActions)

  // Objeto para almacenar las sumas dentro de "data"
  const result = {
    data: {
      disc: 0,
      rect: 0,
      heap: 0,
      dice: 0,
    },
    percentage: {},
  };

  // Recorre el arreglo de objetos y cuenta las selecciones
  filteredActions.forEach(item => {
    const selection = item.details.testName;

    // Verifica si la selección existe en el objeto result.data y suma su valor
    if (Object.prototype.hasOwnProperty.call(result.data, selection)) {
      result.data[selection]++;
    }
  });

  // Calcula los porcentajes y los agrega al objeto result.percentage
  const totalItems = filteredActions.length;
  for (const key in result.data) {
    if (Object.prototype.hasOwnProperty.call(result.data, key)) {
      const count = result.data[key];
      result.percentage[key] = (count / totalItems) * 100;
    }
  }

  return json(result);
}


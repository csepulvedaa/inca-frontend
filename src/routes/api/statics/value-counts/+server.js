// api/statics/value-counts.js
import { json } from '@sveltejs/kit';
import { getFirestore, collection, query, where, getDocs } from 'firebase/firestore';
import { setCache, getCache } from '../../../../../src/lib/cache';

const CACHE_MAX_AGE = import.meta.env.VITE_CACHE_MAX_AGE; // 1 minute

export async function GET({ url }) {
  const selectedSubjectType = String(url.searchParams.get('subject') ?? '');
  const cacheKey = `valueCounts_${selectedSubjectType}`;

  // Try to get data from cache
  const cachedData = getCache(cacheKey, CACHE_MAX_AGE);
  if (cachedData) {
    return json(cachedData);
  }

  // If not in cache, fetch from Firestore
  const db = getFirestore();
  const q = query(collection(db, 'What-is-more1.0'), where('subject', '==', selectedSubjectType));
  const querySnapshot = await getDocs(q);
  const actions = querySnapshot.docs.map(doc => doc.data());

  // Objeto para almacenar las sumas dentro de "data"
  const result = {
    data: {
      correct: 0,
      incorrect: 0,
    },
    percentage: {},
  };

  // Recorre el arreglo de objetos y cuenta las selecciones
  actions.forEach(item => {
    const selection = item.details.result;
    if (selection) {
      result.data['correct']++;
    } else {
      result.data['incorrect']++;
    }
  });

  // Calcula los porcentajes y los agrega al objeto result.percentage
  const totalItems = actions.length;
  for (const key in result.data) {
    if (Object.prototype.hasOwnProperty.call(result.data, key)) {
      const count = result.data[key];
      result.percentage[key] = parseFloat(((count / totalItems) * 100).toFixed(2));
    }
  }

  // Cache the response
  setCache(cacheKey, result);

  return json(result);
}

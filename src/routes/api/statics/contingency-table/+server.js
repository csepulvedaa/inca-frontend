// api/statics/contingency-table.js
import { json } from '@sveltejs/kit';
import { getFirestore, collection, query, where, getDocs } from 'firebase/firestore';
import { setCache, getCache } from '../../../../../src/lib/cache';

const CACHE_MAX_AGE = import.meta.env.VITE_CACHE_MAX_AGE; // 1 minute

export async function GET({ url }) {
  const selectedSubjectType = String(url.searchParams.get('subject') ?? '');
  const cacheKey = `contingencyTable_${selectedSubjectType}`;

  // Try to get data from cache
  const cachedData = getCache(cacheKey, CACHE_MAX_AGE);
  if (cachedData) {
    return json(cachedData);
  }

  // If not in cache, fetch from Firestore
  const db = getFirestore();
  const q = query(collection(db, 'What-is-more1.0'), where('subject', '==', selectedSubjectType));
  const querySnapshot = await getDocs(q);
  const actions = querySnapshot.docs.map(doc => doc.data());

  // Process data to calculate contingency table
  const contingencyTable = calculateContingencyTable(actions);

  // Cache the response
  setCache(cacheKey, contingencyTable);

  return json(contingencyTable);
}

function calculateContingencyTable(data) {
  const contingencyTable = {};

  data.forEach(entry => {
    const result = entry.details.result ? 'True' : 'False'; // Assuming the result is a boolean

    // Initialize the row if it doesn't exist
    if (!contingencyTable[result]) {
      contingencyTable[result] = {};
    }

    // Increment the count for the combination of 'side' and 'result'
    const side = entry.details.side || 'unknown';
    if (!contingencyTable[result][side]) {
      contingencyTable[result][side] = 1;
    } else {
      contingencyTable[result][side]++;
    }
  });

  return contingencyTable;
}
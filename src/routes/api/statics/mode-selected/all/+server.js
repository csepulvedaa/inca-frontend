import { json } from '@sveltejs/kit';
import { actions } from '../../../../../store/cio';

export async function GET({ url }) {
    const selectedActionType = String(url.searchParams.get('type') ?? '');
    const selectedSubjectType = String(url.searchParams.get('subject') ?? '');
    const selectedTeacherType = String(url.searchParams.get('user') ?? '');

    console.log(selectedActionType, selectedSubjectType, selectedTeacherType);

    const filteredActions = actions.filter((action) => {
        const isActionTypeMatch = action.action === 'Mode Selected';

        const isSubjectTypeMatch =
            selectedSubjectType === '' || action.subject === selectedSubjectType;

        // Ambas condiciones deben cumplirse para que la acción pase el filtro
        return isActionTypeMatch && isSubjectTypeMatch;
    });



    const selectionCounts = {};
    let selection;

    for (const entry of filteredActions) {
        if (entry.details && entry.details.selection) {
            selection = entry.details.selection;
            const subject = entry.subject || "No subject";

            if (subject !== "No subject") {
                if (!selectionCounts[subject]) {
                    selectionCounts[subject] = {};
                }

                if (!selectionCounts[subject][selection]) {
                    selectionCounts[subject][selection] = 0;
                }

                selectionCounts[subject][selection]++;
            }
        }
    }

    for (const subject in selectionCounts) {
        const totalCount = Object.values(selectionCounts[subject]).reduce((acc, count) => acc + count, 0);
        for (const selection in selectionCounts[subject]) {
            selectionCounts[subject][selection] = {
                value: selectionCounts[subject][selection],
                percentage: (selectionCounts[subject][selection] / totalCount) * 100
            };
        }
    }

    const selectionCountsJSON = JSON.stringify(selectionCounts, null, 2);
    console.log(selectionCountsJSON);
    return json(selectionCounts);
}


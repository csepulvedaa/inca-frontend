// api/statics/subjects.js
import { json } from '@sveltejs/kit';
import { getFirestore, collection, query, getDocs } from 'firebase/firestore';
import { setCache, getCache } from '../../../../../../src/lib/cache';

const CACHE_MAX_AGE = import.meta.env.VITE_CACHE_MAX_AGE; // 1 minute

export async function GET() {
    const cacheKey = 'subjects';

    // Try to get data from cache
    const cachedData = getCache(cacheKey, CACHE_MAX_AGE);
    if (cachedData) {
        return json(cachedData);
    }

    const db = getFirestore(); // assuming you have initialized Firestore

    // Use Firestore query to get documents from the 'actions' collection
    const q = query(collection(db, 'What-is-more1.0'));
    const querySnapshot = await getDocs(q);
    const actions = querySnapshot.docs.map(doc => doc.data());

    // Create a set to store unique user names
    const usersSet = new Set();

    // Iterate through the actions array and add user names to the set
    actions.forEach((action) => {
        if (action.subject) {
            usersSet.add(action.subject);
        }
    });

    // Convert the set of user names into an array
    const usersArray = Array.from(usersSet);

    // Create the output object
    const output = {
        subjects: usersArray,
    };

    // Cache the response
    setCache(cacheKey, output);

    return json(output);
}

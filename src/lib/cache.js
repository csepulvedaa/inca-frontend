// cache.js
const cache = new Map();

function setCache(key, data) {
  cache.set(key, { data, timestamp: Date.now() });
}

function getCache(key, maxAge) {
  const cachedData = cache.get(key);

  if (cachedData && Date.now() - cachedData.timestamp < maxAge) {
    return cachedData.data;
  }

  // Clear outdated cache
  cache.delete(key);
  return null;
}

export { setCache, getCache };
